"""
Simple mocks
"""
import unittest
from unittest.mock import Mock


class FirstMockTestCase(unittest.TestCase):

    def test_very_simple_mock(self):
        mock = Mock()
        mock.x = 'something'

        foo_mock = Mock(return_value=5)

        self.assertEqual(mock.x, 'something')
        self.assertEqual(foo_mock(), 5)
