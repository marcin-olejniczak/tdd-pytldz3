import unittest
from unittest.mock import patch, Mock, MagicMock

from src.nbp import is_gold_expensive, NbpApi, gold_change_percentage

class NbpTestCase(unittest.TestCase):
    
    def setUp(self):
        self.na = NbpApi()
    
    def test_is_gold_expensive(self):
        with patch.object(NbpApi, 'get_gold_price', return_value=201) as mock:
            self.assertEqual(is_gold_expensive(200), True)

            mock.return_value = 190
            self.assertEqual(is_gold_expensive(200), False)

    def test_show_what_mock_does(self):
        print(self.na.get_gold_price())

        self.na.get_gold_price = Mock(return_value=1000000)

        print(self.na.get_gold_price())

    def test_gold_top_count_called_with(self):
        with patch.object(NbpApi, 'make_request') as mock:
            self.na.get_gold_last_count(10)
            mock.assert_called_with(
                'http://api.nbp.pl/api/cenyzlota/last/10?format=json'
            )

    def test_percentage_change(self):
        imitate_json = [{'cena': 100}, {'cena':110}]
        with patch.object(NbpApi, 'get_gold_last_count',
                          return_value = imitate_json) as mock:
            self.assertEqual(gold_change_percentage(4123), 10)
