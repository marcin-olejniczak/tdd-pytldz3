import unittest

from parameterized import parameterized

from src.fizzbuzz import fizzbuzz
from src.get_days import find_day


class FizzBuzzTest(unittest.TestCase):


    def test_fizzbuzz(self):
        """
        test fizzbuzz function
        :return:
        """
        self.assertEqual(fizzbuzz(3), 'Fizz')
        self.assertEqual(fizzbuzz(5), 'Buzz')
        self.assertEqual(fizzbuzz(15), 'FizzBuzz')
        self.assertEqual(fizzbuzz(30), 'FizzBuzz')
        self.assertEqual(fizzbuzz(4), 4)

    @parameterized.expand(
        [
            ['Test Fizz', 3, 'Fizz'],
            ['Test Buzz', 5, 'Buzz'],
            ['Test FizzBuzz', 15, 'FizzBuzz'],
        ]
    )
    def test_parameterized(self, name, a, b):
        self.assertEqual(fizzbuzz(a), b, name)

    @parameterized.expand(
        [
            ['Test Raises Value Error', 2019, 14, 18, ValueError],
        ]
    )
    def test_parameterized(self, name, y, m, d, exp):

        with self.assertRaises(exp):
            find_day(y, m, d)




