import unittest

""" Find day of he year"""


def find_day(year, month, day):
    count = 0
    if year % 4 == 0 and (year % 100 or year % 400 == 0):
        days_in_month = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    else:
        days_in_month = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

    if month > 12:
        raise ValueError('Jest 12 miesięcy w roku')
    elif day > days_in_month[month - 1]:
        raise ValueError(
            f'W miesiącu {month} jest {days_in_month[month - 1]} dni')

    for days in range(0, month - 1):
        count += days_in_month[days]
    count += day
    return count


class FindDayTest(unittest.TestCase):
    def test_days(self):
        self.assertEqual(find_day(2019, 10, 18), 291)

        # Rok przestępny
        self.assertEqual(find_day(2016, 8, 12), 225)

    def test_bad_month(self):
        with self.assertRaises(ValueError):
            find_day(2019, 14, 18)

    def test_bad_day(self):
        with self.assertRaises(ValueError):
            find_day(2019, 2, 30)
